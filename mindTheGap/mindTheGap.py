import sys
from time import gmtime, strftime
from numpy import *
from matplotlib.pyplot import *
from ESN import *

# *****************************************************************************

# 0 - no, 1 - yes
USE_OUTPUT_FEEDBACK = 0

# *****************************************************************************

dataDir = 'data/'
targetDir = 'target/'
resultsDir = 'results/'

dataFilesList = [1] #[i for i in arange(0, 10, 1)] # indexes of data files to be processed

inScalingList = [0.2] #[i for i in arange(0.1, 1.1, 0.1)] # (0.1, 1)
leakingRateList = [0.6] #[i for i in arange(0.1, 1.1, 0.1)] # (0.1, 1)
spectralRadiusList = [0.5] #[i for i in arange(0.5, 1.6, 0.1)] # (0.5, 1.5)
regList = [1e-8] #[10.0**i for i in arange(-8, 10, 1)] # (10^-8, 10^9)

nrmses = zeros((max(dataFilesList) + 1, len(inScalingList), len(leakingRateList), len(spectralRadiusList), len(regList)))

trainLen = 71250
testLen = 3750

outSize = 1
resSize = 1000

# function to collect X*X.T and Yt*X.T in order to save the memory
def collect_XX_T_and_YX_T(data, inSize, Win, W, Yt, targetColumn, leakingRate):
    XX_T = zeros((1 + inSize + resSize, 1 + inSize + resSize)) 
    YX_T = zeros((XX_T.shape[0]))
	
    if USE_OUTPUT_FEEDBACK == 0:
        data = delete(data, targetColumn, axis=1)
	
    trainLen = data.shape[0]
		
    partsN = 4
    partLen = trainLen // partsN
				
    x = zeros((resSize, 1))
        
    for partIndex in range(partsN):
        if (partIndex + 1) == partsN:
            X = zeros((1 + inSize + resSize, trainLen - partIndex * partLen))
            X, x = runESN(data[partIndex * partLen:], X, x, X.shape[1], Win, W, leakingRate)
    							
            XX_T += dot(X, X.T)
            YX_T += dot(Yt[partIndex * partLen:], X.T)
            del X
    						
        else:
            X = zeros((1 + inSize + resSize, partLen))
            X, x = runESN(data[partIndex * partLen:(partIndex + 1) * partLen], X, x, partLen, Win, W, leakingRate)
    							
            XX_T += dot(X, X.T)
            YX_T += dot(Yt[partIndex * partLen:(partIndex + 1) * partLen], X.T)
            del X
            
    return XX_T, YX_T, x



# function to find the index of the column to be predicted
def findTargetCol(data):
    targetCol = 0
    
    for col in range(data.shape[1]):
        if sum(data[:, col]) == 0:
            targetCol = col
        
    return targetCol



# *****************************************************************************



filedatetime = strftime("%Y-%m-%d %H %M ", gmtime())
if USE_OUTPUT_FEEDBACK == 1:
    filedatetime += 'fb '

save(resultsDir + filedatetime + 'dataFilesList', dataFilesList)
save(resultsDir + filedatetime + 'inScalingList', inScalingList)
save(resultsDir + filedatetime + 'leakingRateList', leakingRateList)
save(resultsDir + filedatetime + 'spectralRadiusList', spectralRadiusList)
save(resultsDir + filedatetime + 'regList', regList)


# loop through data files in 'dataDir' directory
for dataFileIndex in dataFilesList:
    filename = 'a' + str(dataFileIndex).zfill(2) + '.csv'				
    print( '\nProcessing ' + filename + ' data...')
    sys.stdout.flush()
    
    targetSignals = loadtxt(targetDir + filename[0:3] + '.missing.txt')
    std_targetSignals = std(targetSignals) 
					
    # load the data
    data = loadtxt(dataDir + filename, delimiter=',', skiprows=1)
    
    targetCol = findTargetCol(data[trainLen:, 1:])

    normalizedData, means, stds = normalizeData(data[:, 1:])
    
    inputData = delete(normalizedData, targetCol, axis=1)
    
    trainData = normalizedData[:trainLen, :]
    
    # set the corresponding target matrix directly
    Yt = normalizedData[1:trainLen + 1, targetCol]
					
    if USE_OUTPUT_FEEDBACK == 0:
        inSize = normalizedData.shape[1] - 1
    else:
        inSize = normalizedData.shape[1]
    
    random.seed(42)
    Win1, W1 = generateESN(inSize, resSize, neuronConnectivity = 10, inScaling = 1, spectralRadius = 1)
    
    # find the optimal esn parameters values
    for inScalingIndex, inScaling in enumerate(inScalingList):
        Win = Win1 * inScaling
        
        for spectralRadiusIndex, spectralRadius in enumerate(spectralRadiusList):
            W = W1 * spectralRadius
           
            for leakingRateIndex, leakingRate in enumerate(leakingRateList):
                print( 'file= ' + str(dataFileIndex) + ', inSc= ' + str(inScaling) + ', spRad= ' + str(spectralRadius) + ', leakRate= ' + str(leakingRate) ) 
                            					
                XX_T, YX_T, x = collect_XX_T_and_YX_T(trainData, inSize, Win, W, Yt, targetCol, leakingRate)
            					
                for regIndex, reg in enumerate(regList):                    
                    Wout = trainESN(YX_T, XX_T, reg)    
    
                    if USE_OUTPUT_FEEDBACK == 0:
                        testData = inputData[trainLen:]
                        firstInput = inputData[trainLen-1, :]
                    else:
                        testData = normalizedData[trainLen:]
                        firstInput = normalizedData[trainLen-1, :]
                        
                    Y = testESN(testData, firstInput, outSize, Win, W, x, Wout, leakingRate, targetCol, USE_OUTPUT_FEEDBACK)
    
#                    if USE_OUTPUT_FEEDBACK == 0:
#                        #Y = testESN(inputData, trainLen, testLen, outSize, Win, W, x, Wout, leakingRate, targetCol)
#                        Y = testESN(inputData[trainLen:], inputData[trainLen-1, :], outSize, Win, W, x, Wout, leakingRate, targetCol)
#                    else:
#                        #Y = testESN(normalizedData, trainLen, testLen, outSize, Win, W, x, Wout, leakingRate, targetCol, USE_OUTPUT_FEEDBACK)
#                        Y = testESN(normalizedData[trainLen:], normalizedData[trainLen-1, :], outSize, Win, W, x, Wout, leakingRate, targetCol, USE_OUTPUT_FEEDBACK)		
                    # scaling Y to the original form
                    generatedSignals = squeeze(dot(Y, stds[targetCol]) + means[targetCol])
					
                    # normalized root mean square error
                    nrmse = sqrt(mean(square(targetSignals - generatedSignals))) / std_targetSignals               
                    print( 'NRMSE = ' + str(nrmse) + ', with reg = %.0e' % reg )
                    
                    nrmses[dataFileIndex, inScalingIndex, leakingRateIndex, spectralRadiusIndex, regIndex] = nrmse
                    
                sys.stdout.flush()
    
            # save matrices for later using
            save(resultsDir + filedatetime + 'nrmses', nrmses)