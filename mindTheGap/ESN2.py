from numpy import *
import scipy.linalg
import scipy.sparse
import scipy.sparse.linalg

def generateESN(inSize, resSize, neuronConnectivity = 10, inScaling = 1, spectralRadius = 1):
    Win = (random.rand(resSize, 1 + inSize) - 0.5) * inScaling
    W = scipy.sparse.rand(resSize, resSize, density = neuronConnectivity / resSize)
    W.data = W.data - 0.5
    
    # normalizing and setting spectral radius:
    rhoW = max(abs(scipy.sparse.linalg.eigs(W)[0]))
    W *= spectralRadius / rhoW
            
    return Win, W
    
def runESN(data, X, x, esnObj):
    Win = esnObj.getWin()
    W = esnObj.getW()
    leakingRate = esnObj.getLeakingRate()
    
    trainLen = data.shape[0]
    
    # run the reservoir with the data and collect X
    for t in range(trainLen):
        u = vstack(data[t, :])
        x = (1 - leakingRate) * x + leakingRate * tanh(dot(Win, vstack((1, u))) + W.dot(x))
        X[:, t] = vstack((1, u, x))[:, 0]
            
    return X, x

def trainESN(YX_T, XX_T, reg):    
    Wout = dot(YX_T, scipy.linalg.inv(XX_T + \
        reg * eye(XX_T.shape[0])))
        
    return Wout
    
def testESN(data, firstInput, x, esnObj, targetCol, USE_OUTPUT_FEEDBACK = 0):
    Win = esnObj.getWin()
    W = esnObj.getW()
    Wout = esnObj.getWout()
    leakingRate = esnObj.getLeakingRate()
    
    outSize = Wout.shape[0]
    testLen = data.shape[0] 
    
    Y = zeros((outSize, testLen))
    
    u = vstack(firstInput)
    
    for t in range(testLen):
        y = dot(Wout, vstack((1, u, x)))
        Y[:, t] = y
        
        u = vstack(data[t, :])
        if USE_OUTPUT_FEEDBACK == 1:
            u[targetCol] = y
        
        x = (1 - leakingRate) * x + leakingRate * tanh(dot(Win, vstack((1, u))) + W.dot(x))

    return Y

def normalizeData(data):
    means = mean(data, axis=0)
    stds = std(data, axis=0)
    
    for i in range(data.shape[1]):
        data[:, i] = (data[:, i] - means[i]) / stds[i]
    
    return data, means, stds