import sys
import numpy
import pylab
import scipy
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

filedatetime = "2016-03-22 21 35"

# load the data
inScalingList = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " inScalingList.npy")
leakingRateList = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " leakingRateList.npy")
spectralRadiusList = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " spectralRadiusList.npy")
regList = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " regList.npy")
[x, y] = scipy.meshgrid(inScalingList, spectralRadiusList)

# nrmses = zeros((len(dataFilesList), len(inScalingList), len(leakingRateList), len(spectralRadiusList), len(regList)))
nrmses = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " nrmses.npy")

nrmsesMinReg = nrmses.min(axis=4)
nrmsesAvgMinReg = numpy.mean(nrmsesMinReg, axis=0)

# find leakingRate with the minimum nrmse
minNrmse = sys.maxsize
minLeakingRateIndex = 0
for i in range(nrmses.shape[2]):
    nrmse = nrmsesAvgMinReg[:, i, :].min()
    if nrmse < minNrmse:
        minNrmse = nrmse
        minLeakingRateIndex = i

z = nrmsesAvgMinReg[:, minLeakingRateIndex, :]

# find the indexes of the parameters with the least nrmse
minInScalingIndex, minSpectralRadiusIndex = numpy.where(z == z.min())

print("minInScaling = " + str(inScalingList[minInScalingIndex[0]]))
print("minLeakingRate = " + str(leakingRateList[minLeakingRateIndex]))
print("minSpectralRadius = " + str(spectralRadiusList[minSpectralRadiusIndex[0]]))

fig = pylab.figure()
ax = fig.gca(projection='3d')
ax.set_xlabel("inScaling")
ax.set_ylabel("spectralRadius")
ax.set_zlabel("nrmse")
surf = ax.plot_surface(
    x, y, z.T, rstride=1, cstride=1,
    cmap=cm.jet, #(z.T/z.max()),
    linewidth=0, antialiased=False, shade=False)
pylab.show()

# plot x, y, z values
#fig = pylab.figure()
#ax = Axes3D(fig)
#
#ax.set_xlabel("inScaling")
#ax.set_ylabel("spectralRadius")
#ax.plot_wireframe(x,y,z.T) #facecolors=cm.jet(z/z.max()))
##ax.set_zlim3d(0,10)
#pylab.show()