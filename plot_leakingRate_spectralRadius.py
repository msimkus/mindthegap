import sys
import numpy
import pylab
import scipy
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

filedatetime = "2016-03-22 21 41 fb"

# load the data
inScalingList = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " inScalingList.npy")
leakingRateList = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " leakingRateList.npy")
spectralRadiusList = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " spectralRadiusList.npy")
regList = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " regList.npy")
[x, y] = scipy.meshgrid(leakingRateList, spectralRadiusList)

# nrmses = zeros((len(dataFilesList), len(inScalingList), len(leakingRateList), len(spectralRadiusList), len(regList)))
nrmses = numpy.load("C:/Users/User/Desktop/results/" + filedatetime + " nrmses.npy")

nrmsesMinReg = nrmses.min(axis=4)
nrmsesAvgMinReg = numpy.mean(nrmsesMinReg, axis=0)

# find leakingRate with the minimum nrmse
minNrmse = sys.maxsize
minInScalingIndex = 0
for i in range(nrmses.shape[1]):
    nrmse = nrmsesAvgMinReg[i, :, :].min()
    if nrmse < minNrmse:
        minNrmse = nrmse
        minInScalingIndex = i

z = nrmsesAvgMinReg[minInScalingIndex, :, :]

# find the indexes of the parameters with the least nrmse
minLeakingRateIndex, minSpectralRadiusIndex = numpy.where(z == z.min())

print("minInScaling = " + str(inScalingList[minInScalingIndex]))
print("minLeakingRate = " + str(leakingRateList[minLeakingRateIndex[0]]))
print("minSpectralRadius = " + str(spectralRadiusList[minSpectralRadiusIndex[0]]))

fig = pylab.figure()
ax = fig.gca(projection='3d')
ax.set_xlabel("leakingRate")
ax.set_ylabel("spectralRadius")
ax.set_zlabel("nrmse")
surf = ax.plot_surface(
    x, y, z.T, rstride=1, cstride=1,
    cmap=cm.jet, #(z.T/z.max()),
    linewidth=0, antialiased=False, shade=False)
pylab.show()

# plot x, y, z values
#fig = pylab.figure()
#ax = Axes3D(fig)
#
#ax.set_xlabel("leakingRate")
#ax.set_ylabel("spectralRadius")
#ax.plot_wireframe(x,y,z.T) #facecolors=cm.jet(z/z.max()))
##ax.set_zlim3d(0,10)
#pylab.show()